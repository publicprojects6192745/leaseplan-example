package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for cola")
    public void heSeesTheResultsDisplayedForCola() {
        restAssuredThat(response -> response.body("title", hasItem(containsString("cola"))));
    }

    @Then("he doesn't not see the results")
    public void he_Doesnt_Not_See_The_Results() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }
}
