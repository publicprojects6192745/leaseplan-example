# What was refactored and Why?

## post_product.feature
- The `name` of the Scenario was missing, making it not visible in the `Serenity Report`.
- Later the `Scenario` was split into `3 Scenarios` to have `two positive` and `one negative`
```Gherkin
   When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
   Then he sees the results displayed for apple
```

1. In the above snippet, the get request was made for `orange`, 
2. But the results was checked for `apple`

```Gherkin
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the results displayed for mango
```

1. In the above snippet, the get request was made for `apple`,
2. But the results was checked for `mango` which is not present in the available products
3. Available products: `"orange"`, `"apple"`, `"pasta"`, `"cola"`

```Java
@Then("he doesn not see the results")
public void he_Doesn_Not_See_The_Results() {
restAssuredThat(response -> response.body("error", contains("True")));
}
```

was modified to:
```Java
@Then("he doesn't not see the results")
public void he_Doesnt_Not_See_The_Results() {
restAssuredThat(response -> response.body("detail.error", equalTo(true)));
}
```

1. To get the value of the error, `"error"` was changed to `"detail.error"` to respect the JSON path
```JSON
{"detail":{"error":true,"message":"Not found","requested_item":"car","served_by":"https://waarkoop.com"}}
```
2. The value of the error was compared with `"True"` which is a `String`.
   Was modified to verify the equality with `true` as `boolean`

###### CarAPI class was removed as it was not used
### How to run the tests
1. mvn clean install  `to integrate all changes`
2. mvn clean verify   `to exectute the Test Runner`

## Pipelines
#### .gitlab-ci.yml was added to run the pipeline on Gitlab
#### Steps
1. Java install
2. Maven install
3. Run tests
4. Set the Report to be visible on Gitlab pages

The following command was added to be able to view the report on Gitlab pages
```
artifacts:
paths:
- target/site/serenity
```

- example of a report can be found on : https://javaintellijprojectspersonal.gitlab.io/-/leaseplan-example/-/jobs/5501693574/artifacts/target/site/serenity/index.html
![Local Image](src/main/resources/gitlab_report.png)

### Write new tests
1. Define a Scenario with `Given`, `When`, `Then`
2. Implement the Steps in the StepDefinition java class
- Each method should have an annotation, the text inside it should match that text from the feature file